import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';

import { StateService } from '../services/state-service';
import { DataService } from '../services/data-service';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;
  stateService: StateService;
  dataService: DataService;
  bgclass: String = 'homepage';
  loadstate: String = 'loading';
  stateSubscription;
  dataSubscription;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, stateService: StateService, dataService: DataService) {
    this.stateService = stateService;
    this.dataService = dataService;
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  ngOnInit() {
    this.stateSubscription = this.stateService.navChanged.subscribe(
      () => {
        this.bgclass = this.stateService.bgclass;
      }
    )

    this.dataSubscription = this.dataService.dataChanged.subscribe(
      () => {
        this.loadstate = 'loaded';
      }
    )
  }

  goToSettings() {
    this.stateService.changeRootBg('settings');
  }

  goToHome() {
    this.stateService.changeRootBg('homepage');
  }
}
