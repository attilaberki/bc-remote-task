import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/subject';

@Injectable()
export class StateService {
  bgclass: String = 'mainpage';
  navChanged = new Subject<void>();

  constructor() {}

  changeRootBg(className) {
    this.bgclass = className;
    this.navChanged.next();
  }

}
