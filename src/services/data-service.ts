import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Subject } from 'rxjs/subject';
import 'rxjs/add/operator/map';


@Injectable()
export class DataService {
  private articles = [];
  private url = 'https://attilaberki.bitbucket.io/budacode/data.json';
  dataChanged = new Subject<void>();
  http: Http;

  constructor(http: Http) {
    this.http = http;
  }

  fetchData() {
    this.http.get(this.url)
    .map((response: Response) => {
      const data = response.json();
      const extarts = data.articles;
      const imgpath = data.imgpath;
      const nwarts = extarts.map((art) => {
        return {...art, image: `${imgpath}${art.image}`};
      })
      return nwarts;
    })
    .subscribe(
      (data) => {
        this.articles = data;
        //setTimeout(()=>{this.dataChanged.next();}, 5000)
        this.dataChanged.next();
      }
    );
  }

  getArticles() {
    return this.articles;
  }

}
