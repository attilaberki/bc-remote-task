import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/subject';

@Injectable()
export class SettingsService {
  settings = {
    twitter: true,
    facebook: false
  };
  settingsChanged = new Subject<void>();

  constructor() {}

  changeTwitter(bool) {
    this.settings.twitter = bool;
    this.settingsChanged.next();
  }

  changeFacebook(bool) {
    this.settings.facebook = bool;
    this.settingsChanged.next();
  }

  getSettings() {
    return this.settings;
  }

}
