import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Settings } from '../settings/settings';

import { StateService } from '../../services/state-service';
import { DataService } from '../../services/data-service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  articles = [];
  stateService: StateService;
  dataService: DataService;
  loadstate: String = 'loading';
  stateSubscription;
  dataSubscription;

  constructor(public navCtrl: NavController, stateService: StateService, dataService: DataService) {
    this.stateService = stateService;
    this.dataService = dataService
  }

  ngOnInit() {
    this.stateSubscription = this.stateService.navChanged.subscribe(
      () => {
        if (this.stateService.bgclass == 'settings') {
          this.navCtrl.push(Settings, {}, {
            animation: 'ios-transition',
            direction: 'back',
            duration: 1000,
            easing: 'linear'
          });
        }
      }
    );

    this.dataSubscription = this.dataService.dataChanged.subscribe(
      () => {
        this.articles = this.dataService.getArticles();
        this.loadstate = 'loaded';
      }
    )

    this.dataService.fetchData();
  }

}
