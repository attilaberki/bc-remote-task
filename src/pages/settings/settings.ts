import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { StateService } from '../../services/state-service';
import { SettingsService } from '../../services/settings-service';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class Settings {
  stateService: StateService;
  settingsService: SettingsService;
  stateSubscription;
  settingsSubscription;
  settings = {};
  twitter: Boolean = true;
  facebook: Boolean = false;

  constructor(public navCtrl: NavController, stateService: StateService, settingsService: SettingsService) {
    this.stateService = stateService;
    this.settingsService = settingsService;
  }

  ngOnInit() {
    this.stateSubscription = this.stateService.navChanged.subscribe(
      () => {
        if (this.stateService.bgclass == 'homepage') {
          this.navCtrl.pop({
            animation: 'ios-transition',
            direction: 'forward',
            duration: 1000
          });
        }
      }
    )

    this.settingsSubscription = this.settingsService.settingsChanged.subscribe(
      () => {
        this.settings = this.settingsService.getSettings();
      }
    )

    this.settings = this.settingsService.getSettings();
  }

  twitterChange(bool) {
    this.settingsService.changeTwitter(bool);
  }

  facebookChange(bool) {
    this.settingsService.changeFacebook(bool)
  }

  ngOnDestroy() {
    this.stateSubscription.unsubscribe();
    this.settingsSubscription.unsubscribe();
  }

}
